﻿using UnityEngine;
using System.Collections;

public class BombScript : MonoBehaviour
{
    
    private float myCountdown;
	// Use this for initialization
	void Start ()
	{
	    myCountdown = Random.Range(2, 5);
	}
	
	// Update is called once per frame
	void Update ()
	{
	    myCountdown -= Time.deltaTime;

	    if (myCountdown < 0)
	    {
	        Destroy(gameObject);
	    }
	}
}
