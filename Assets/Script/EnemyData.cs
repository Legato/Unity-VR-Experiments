﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class EnemyData : MonoBehaviour
{
    public short startHp;
    public short maxHp;


    private short StartHp
    {
        get
        {
            return startHp;
        }
        set
        {
            if (value > maxHp)
            {
                startHp = maxHp;
            }
            else if (value < 0)
            {
                startHp = 0;
            }
            else
            {
                startHp = value;
            }
        }
    }


    private short MaxHp
    {
        get
        {
            return maxHp;
        }
        set
        {
            if (maxHp < 0)
            {
                maxHp = 0;
            }

            if (startHp > maxHp)
            {
                startHp = maxHp;
            }
        }
    }

    void Update()
    {
        StartHp = startHp;
        MaxHp = maxHp;
    }
}
