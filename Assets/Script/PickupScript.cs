﻿using UnityEngine;
using System.Collections;
using Valve.VR;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class PickupScript : MonoBehaviour
{
    private SteamVR_TrackedObject trackedObject;
    private SteamVR_Controller.Device device;


    void Awake ()
	{
	    trackedObject = GetComponent<SteamVR_TrackedObject>();
	}
	
	void FixedUpdate ()
	{
	    device = SteamVR_Controller.Input((int) trackedObject.index);

	    if (device.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
	    {
	        Debug.Log("You are holding down the trigger.");
	    }
	}

    void OnTriggerStay(Collider col)
    {
        Debug.Log("You have collided with " + col.name + " and activatet OnTriggerStay.");

        if (device.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
        {
            GrabObject(col);
        }
        if (device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
        {
            ReleaseObject(col);
            TossObject(col.attachedRigidbody);
        }
    }

    private void GrabObject(Collider col)
    {
        Debug.Log("You have collided with " + col.name + " while holding down trigger.");
        col.attachedRigidbody.isKinematic = true;
        col.gameObject.transform.SetParent(gameObject.transform);
    }

    private void ReleaseObject(Collider col)
    {
        Debug.Log("You have released Touch while colliding with" + col.name);
        col.gameObject.transform.SetParent(null);
        col.attachedRigidbody.isKinematic = false;
    }

    private void TossObject(Rigidbody rigidBody)
    {
        Transform origin = trackedObject.origin ? trackedObject.origin : trackedObject.transform.parent;

        if (origin != null)
        {
            rigidBody.velocity = origin.TransformVector(device.velocity);
            rigidBody.angularVelocity = origin.TransformVector(device.angularVelocity);
        }
        else
        {
            rigidBody.velocity = device.velocity;
            rigidBody.angularVelocity = device.angularVelocity;
        }
        
    }
}
