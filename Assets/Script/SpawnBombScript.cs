﻿using UnityEngine;
using System.Collections;

public class SpawnBombScript : MonoBehaviour {

    public Transform bomb;
    public Vector2 SpawnRate = new Vector2(0f,1f);
    public Vector2 SpawnPositionX = new Vector2(-0.25f,0.25f);
    public Vector2 SpawnPositionZ = new Vector2(-0.25f, 0.25f);
    private float mySpawnTimer;
    // Use this for initialization
    void Start ()
    {
        SetSpawnTime();
    }
	
    
	// Update is called once per frame
	void Update ()
	{
	    mySpawnTimer -= Time.deltaTime;

	    if (mySpawnTimer < 0)
	    {
	        Instantiate(bomb, new Vector3(Random.Range(SpawnPositionX.x, SpawnPositionX.y), 10, Random.Range(SpawnPositionZ.x, SpawnPositionZ.y)), Quaternion.identity);
            SetSpawnTime();
	    }
	}

    private void SetSpawnTime()
    {
        mySpawnTimer = Random.Range(SpawnRate.x, SpawnRate.y);
    }
}
